import React, { useState, useEffect, useCallback } from 'react';
import MomentUtils from '@date-io/moment';
import moment from 'moment';
import { MuiPickersUtilsProvider } from '@material-ui/pickers';
import { Modal, CardContent, Grid } from '@material-ui/core';
import { SnackbarProvider } from 'notistack';
import api from './services/api';
import TabletAndMobileView from './components/TabletAndMobileView';
import DesktopView from './components/DesktopView';
import { useMediaQuery } from 'react-responsive';
import OrderForm from './components/OrderForm';
import Loader from './components/Loader';
import 'moment/locale/fi';

const App = () => {
	const [data, setData] = useState(null);
	const [loading, setLoading] = useState(false);
	const [currentDate, setCurrentDate] = useState(moment());
	const [prevDate, setPrevDate] = useState(moment());
	const [selectedItems, setSelectedItems] = useState([]);
	const [openModal, setOpenModal] = useState(false);
	const isTabletOrMobileDevice = useMediaQuery({
		query: '(max-width: 1224px)'
	});

	const fetchData = useCallback(() => {
		setLoading(true);
		const cloneCurrentDate = currentDate.clone();
			api.getData(cloneCurrentDate.format('M-D-YYYY'), cloneCurrentDate.add(1, 'days').format('M-D-YYYY'))
				.then(data => {
					setData(data)
					setPrevDate(currentDate)
					setLoading(false);
				})
	}, [currentDate])

	useEffect(() => {
		let mounted = true;
		if (mounted) {
			fetchData()
		}

		return () => {
			mounted = false;
		}
	}, [fetchData])

	const setPrevDay = () => {
		setCurrentDate(prevDate => moment(prevDate).add(-1, 'days'))
	}

	const setNextDay = () => {
		setCurrentDate(prevDate => moment(prevDate).add(1, 'days'))
	}

	const handleDateChange = date => {
		setCurrentDate(moment(date))
	}

	const setTodayDay = () => {
		setCurrentDate(moment());
	}

	const handleSelectedItems = (item, isItemSelected) => {
		item.Date = curDate.format('YYYY-MM-DDT00:00:00');
		setSelectedItems(prevItems => {
			return isItemSelected ? prevItems.filter(el => el.TimeSlotId !== item.TimeSlotId) : [...prevItems, item];
		})
	}

	const removeSelectedItem = (id) => {
		setSelectedItems(prevItems => {
			return prevItems.filter(item => item.TimeSlotId !== id);
		})
	}

	const handleOpenModal = () => {
		setOpenModal(true);
	}

	const handleCloseModal = () => {
		setOpenModal(false);
	}

	const removeAllSelectedItems = () => {
		setSelectedItems([]);
	}

	if (!data) return (
		<div style={{
			position: 'fixed',
			zIndex: 1300,
			right: 0,
			left: 0,
			top: 0,
			bottom: 0,
			background: 'lightgrey'
		}}>
			<div style={{
				display: 'flex',
				justifyContent: 'center',
				alignItems: 'center',
				height: '100%'
			}}>
				<Loader />
			</div>
		</div>
	)

	const curDate = prevDate.format('YYYY-MM-DDT00:00:00') in data['Calendar'] ? prevDate : currentDate;
	const fullData = data["Calendar"][`${curDate.format('YYYY-MM-DDT00:00:00')}`];
	const isToday = prevDate.format('YYYY-MM-DD') === moment().format('YYYY-MM-DD');
	// const from = parseInt(fullData[0].StartTime);
	const from = 9;
	const to = parseInt(fullData[fullData.length - 1].EndTime);
	const isDateEqual = currentDate.format('YYYY-MM-DD') === prevDate.format('YYYY-MM-DD');
	const maxDate = moment('2024-09-16');
	// const maxDate = moment().add(1, 'y').set({
	// 	'M': 11,
	// 	'D': 30
	// });
	const rest = {
		maxDate,
		data: fullData,
		from,
		to,
		setPrevDay,
		setNextDay,
		handleDateChange,
		handleOpenModal,
		setTodayDay,
		currentDate: curDate,
		isToday,
		selectedItems,
		handleSelectedItems,
	}

	return (
		<MuiPickersUtilsProvider utils={MomentUtils}>
			<SnackbarProvider>
				<CardContent>
					<Grid container spacing={3}>
						<Grid item lg={6} xs={12}>
							<p>Voit varata harrastevuoron tästä kalenterista. Keilavuoro
								maksetaan suoraan keilahallille ja pallopelivuorot voit maksaa
								keilahallille, vastaanottoon tai hoitovastaanottoon R-Kioskin
								yhteydessä.</p>
							<p>Vuoron voit perua kuluitta 3 h ennen vuoron alkua.
								Osakashinnan saat näyttämällä osakaskortin maksun
								yhteydessä.</p>
							<p>Huomioithan, että Viikko-osakkeen harrastevuoroja ei voi
								käyttää netissä varattuun vuoroon vaan vaihdot on tehtävä
								puhelimitse.</p>
						</Grid>
					</Grid>
				</CardContent>
				{isTabletOrMobileDevice ? (
					<TabletAndMobileView {...rest} />
				) : (
					<DesktopView {...rest} />
				)}
				{openModal ? (
					<OrderForm
						removeAllSelectedItems={removeAllSelectedItems}
						currentDate={curDate}
						openModal={openModal}
						handleCloseModal={handleCloseModal}
						selectedItems={selectedItems}
						removeSelectedItem={removeSelectedItem}
						fetchData={fetchData}
					/>
				) : null}
				<Modal
					open={!isDateEqual || loading}
				>
					<div
						style={{
							display: 'flex',
							justifyContent: 'center',
							alignItems: 'center',
							height: '100%',
							fontSize: 36,
							fontWeight: 'bold',
							color: '#fff'
						}}
					>
						<Loader />
					</div>
				</Modal>
			</SnackbarProvider>
		</MuiPickersUtilsProvider>
	)
}

export default App;
