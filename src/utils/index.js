import {func} from 'prop-types';

function transformSelectedItems(arr, date, prices) {
    return arr.map(item => {
        const [price] = prices['Prices'][item.TimeSlotId].filter((priceItem) => {
            if (item.locationInfo.Title === 'Padel outside') {
                if (parseInt(item.StartTime) < 15) {
                    return priceItem.Price === 26;
                }

                return priceItem.Price === 30;
            }

            return priceItem.PricingType === 'Hinta';
        });
        return {
            "TimeSlotId": item.TimeSlotId,
            "Date": item.Date || date.format('YYYY-MM-DDT00:00:00'),
            "StartTime": item.StartTime,
            "EndTime": item.EndTime,
            "ResourceId": item.Resources[0].Id,
            "Cost": `${price.Price} ${price.PricingType}`,
            "Resources": [
                {
                    "Id": item.Resources[0].Id,
                    "Name": item.Resources[0].Name,
                }
            ],
            "locationInfo": {
                "id": item.locationInfo["Id"],
                "address": "",
                "title": item.locationInfo["Title"]
            },
            "ReserveOwner": ""
        }
    })
}

function TimeSlotFixReducer(item) {
    item.available = true;

    if (item.locationInfo.Title === 'Outdoor Tennis' && item.Resources[0].Name === 'Padel-sisäkenttä 2') {
        item.available = false
    }

    if (item.Resources[0].Name.split(' ')[0] === 'E-Fatbike') {
        item.locationInfo.Title = 'E-Fatbike'
    }

    if (item.Resources[0].Name === 'Pickleball') {
        if (parseInt(item.StartTime) > 21) {
            item.Status = 2
        }
    }

    if (item.locationInfo.Title === 'Squash') {
        const titleArray = item.Resources[0].Name.split(' ');
        const resourceNumber = parseInt(titleArray[titleArray.length - 1], 10);
        if (!isNaN(resourceNumber) && resourceNumber > 1) {
            item.Status = 2
        }
    }

    if (item.locationInfo.Title === 'Tennis') {
        const titleArray = item.Resources[0].Name.split(' ');
        const resourceNumber = parseInt(titleArray[titleArray.length - 1], 10);
        if (!isNaN(resourceNumber) && resourceNumber > 5) {
            item.Status = 2
        }
    }

    return item;
}

function PriceFixReducer(item) {
    if (!item.StandardPrice) {
        item.StandardPrice = 0;
    }

    item.StandardPriceStr = `${item.StandardPrice} €`;

    if (!item.StandardPrice && item.locationInfo.Title === 'Outdoor Tennis') {
        item.StandardPriceStr = '';
    }

    if (item.locationInfo.Title === 'Padel outside') {
        if (parseInt(item.StartTime) < 15) {
            item.StandardPrice = 26;
            item.StandardPriceStr = '26 €';
        } else {
            item.StandardPrice = 30;
            item.StandardPriceStr = '30 €';
        }
    }

    if (item.locationInfo.Title === 'Jopo') {
        item.StandardPriceStr = '15€ 3h';
    }

    return item;
}

export {
    transformSelectedItems,
    TimeSlotFixReducer,
    PriceFixReducer
}
