import React, { useState, useEffect } from 'react';
import {
    Modal,
    Card,
    CardHeader,
    Divider,
    CardContent,
    CardActions,
    Grid,
    Button,
    Checkbox,
    CircularProgress,
    IconButton
} from '@material-ui/core';
import { useSnackbar } from 'notistack';
import InputItem from '../InputItem';
import clsx from 'clsx';
import { Formik, Form, FieldArray } from 'formik';
import { makeStyles, FormControlLabel } from '@material-ui/core';
import RadioCard from '../RadioCard';
import VirtualizedList from '../VirtualizedList';
import { transformSelectedItems } from '../../utils';
import Loader from '../Loader';
import * as Yup from 'yup';
import api from '../../services/api';
import CloseIcon from '@material-ui/icons/Close';
import {func} from 'prop-types';

const useStyles = makeStyles(theme => ({
    root: {
        position: 'absolute',
        top: '50%',
        left: '50%',
        width: '80%',
        maxWidth: '100%',
        maxHeight: '100%',
        transform: 'translate(-50%, -50%)',
        outline: 'none',
        overflowY: 'auto',
    },
    cardContent: {
        [theme.breakpoints.down('md')]: {
            padding: 10
        }
    },
    header: {
        [theme.breakpoints.down('md')]: {
            fontSize: 21,
            fontFamily: 'MetaPro',
        }
    },
    actions: {
        justifyContent: 'flex-end',
        paddingLeft: 20,
        paddingRight: 20,
        [theme.breakpoints.down('md')]: {
            padding: 10
        },
        [theme.breakpoints.down('sm')]: {
            alignItems: 'flex-end',
            flexDirection: 'column'
        }
    },
    label: {
        marginRight: 15
    },
    inputWrapper: {
        display: 'flex',
        [theme.breakpoints.down('md')]: {
            flexDirection: 'column'
        }
    },
    inputRow: {
        marginBottom: 24,
    },
    textItem: {
        display: 'flex',
        alignItems: 'center',
        width: '22%'
    },
    inputLabel: {
        display: 'flex',
        alignItems: 'center',
        width: '16%',
        [theme.breakpoints.down('md')]: {
            width: '100%',
        }
    },
    textareaLabel: {
        display: 'flex',
        alignItems: 'center',
        width: '33%',
        [theme.breakpoints.down('md')]: {
            width: '100%',
        }
    },
    inputItem: {
        position: 'relative',
        width: '84%',
        padding: 5,
        [theme.breakpoints.down('md')]: {
            width: '100%',
            padding: '5px 0'
        }
    },
    inputHint: {
        position: 'absolute',
        top: '-14px',
        right: 0,
        fontSize: '12px',
        lineHeight: '14px',
        [theme.breakpoints.down('md')]: {
            position: 'relative',
            top: 'auto',
            marginBottom: 5
        }
    },
    textareaItem: {
        width: '67%',
        padding: 5,
        [theme.breakpoints.down('md')]: {
            width: '100%',
            padding: '5px 0'
        }
    },
    inputStyle: {
        width: '100%',
        padding: '8px 12px',
        fontFamily: 'MetaPro',
        fontSize: 16,
        background: '#fafafa',
        borderRadius: 3,
        color: '#1d1d1b',
        outline: 'none',
        [theme.breakpoints.down('md')]: {
            color: '#000'
        },
        '&:disabled': {
            background: 'rgba(0, 0, 0, 0.15)'
        }
    },
    error: {
        paddingLeft: '18%',
        color: 'red',
        [theme.breakpoints.down('md')]: {
            padding: '5px 0'
        }
    },
    lastNameError: {
        paddingTop: 5,
        color: 'red',
        [theme.breakpoints.down('md')]: {
            padding: '5px 0'
        }
    },
    submit: {
        fontFamily: 'MetaPro',
        fontSize: '16px',
        background: '#c00000',
        color: '#fff',
        transition: '.25s ease-in-out',
        '&:hover': {
            background: '#c00000',
        },
        '&.Mui-disabled': {
            color: '#fff'
        }
    },
    formControlLabelCheckbox: {
        fontSize: 18,
        fontFamily: 'MetaPro',
        [theme.breakpoints.down('sm')]: {
            fontSize: 14
        }
    },
    radio: {
        padding: 5,
        color: '#c5c5c5'
    },
    progress: {
        color: 'rgb(192, 0, 0)'
    },
    disabledSubmitButton: {
        color: '#fff',
        opacity: '0.3'
    },
    customGridContainer: {
        alignItems: 'stretch'
    },
    customGridItem: {
        paddingTop: '0 !important',
        paddingBottom: '0 !important'
    },
    gridRadioContainer: {
        flexDirection: 'row',
        paddingLeft: '18%',
        [theme.breakpoints.down('sm')]: {
            paddingLeft: 12,
            paddingRight: 12
        }
    },
    gridRadio: {
        flexBasis: 'auto'
    },
    cardHeaderAction: {
        marginTop: 0,
        marginRight: 0
    },
    closeButton: {
        padding: 6
    },
    cardHeaderRoot: {
        [theme.breakpoints.down('md')]: {
            padding: 8
        }
    }
}));

const ValidationSchema = Yup.object().shape({
    lastName: Yup.string()
        .required('* - Pakollinen kenttä'),
    firstName: Yup.string()
        .required('* - Pakollinen kenttä'),
    number: Yup.string()
    .transform(function(value) {
        if (value.charAt(0) === '0') {
            value = `+358${value.substr(1)}`
        }
        return value;
    })
        .required('* - Pakollinen kenttä')
        .matches(/^(\+?(358)[.\s-]?[0-9]{2}?[.\s-]?[0-9]{3}?[.\s-]?[0-9]{2}?[.\s-]?[0-9]{2}?)$|(\+?(380)[.\s-]?[0-9]{2}?[.\s-]?[0-9]{3}?[.\s-]?[0-9]{2}?[.\s-]?[0-9]{2}?)$/, 'kirjoita matkapuhelinnumero kansainvälisessä muodossa (+358xxxxxxxxxx)'),
    sendEmail: Yup.bool(),
    email: Yup.string()
        .email('Väärä sähköpostiosoite'),
    // email: Yup.string()
    //     .when('sendEmail', {
    //         is: (sendEmail) => sendEmail === true,
    //         then: Yup.string()
    //             .email('Väärä sähköpostiosoite')
    //             .required('Pakollinen kenttä')
    //     })
})

const responseMsg = {
    success: 'Kiitos tekemästäsi harrastevarauksesta. Saat hetken kuluttua vahvistuksen tekstiviestinä matkapuhelimeesi ja sähköpostina, mikäli annoit sen tiedot. Voit nyt varata seuraavan vuoron, mikäli haluat. Poistu painamalla "OK".'
}

const OrderForm = ({ openModal, handleCloseModal, selectedItems, currentDate, removeSelectedItem, fetchData, removeAllSelectedItems }) => {
    const classes = useStyles();
    const [prices, setPrices] = useState(null);
    const [requestForm, setRequestForm] = useState(false);
    const { enqueueSnackbar, closeSnackbar } = useSnackbar();

    const responseAction = key => (
        <Button onClick={() => { closeSnackbar(key) }}>OK</Button>
    )

    useEffect(() => {
        let mounted = true;

        if (mounted) {
            const selectedItemsID = selectedItems.map(item => item.TimeSlotId).join(',');

            api.getPrices(selectedItemsID)
                .then(prices => {
                    const res = {Prices: {}};
                    for (let key in prices.Prices) {
                        if (!prices.Prices[key].length) {
                          prices.Prices[key].push({
                            PricingType: 'Hinta',
                            Price: 0,
                            PriceRepresentation: '€',
                          });
                        }
                        res.Prices[key] = prices.Prices[key].filter(item => item.PricingType !== 'Osakashinta' && item.PricingType !== 'Osakashohto')
                    }

                    setPrices(res);
                })
        }
    }, []);

    if (selectedItems.length === 0) {
        handleCloseModal();
    }

    return (
        <Modal
            onClose={handleCloseModal}
            open={openModal}
        >
            {prices ? (
                <Card
                    className={clsx(classes.root)}
                >
                    <Formik
                        // enableReinitialize
                        initialValues={{
                            lastName: "",
                            firstName: "",
                            company: "",
                            email: "",
                            number: "",
                            sendEmail: true,
                            sendSms: true,
                            additionalInformation: '',
                            "Hinta/Osakashinta": "False",
                            ReserveOwner: "",
                            orderInfo: transformSelectedItems(selectedItems, currentDate, prices)
                        }}
                        validationSchema={ValidationSchema}
                        onSubmit={(values) => {
                            setRequestForm(true);
                            const formData = {
                                "IsUnAuth": true,
                                "FirstName": values.firstName,
                                "LastName": values.lastName,
                                "Email": values.email,
                                "SendEmail": values.sendEmail,
                                "SendSms": values.sendSms,
                                "Phone": values.number,
                                "Company": values.company,
                                "buyerComment2": values.additionalInformation,
                                "orderInfo": values.orderInfo.map(item => {
                                    return {
                                        ...item,
                                        "Cost": parseInt(item.Cost),
                                        "ReserveOwner": values.ReserveOwner
                                    }
                                })
                            };

                            api.requestForm(formData)
                                .then(status => {
                                    if (status.success) {
                                        removeAllSelectedItems();
                                        fetchData();
                                        setRequestForm(false);
                                        enqueueSnackbar(responseMsg.success, {
                                            variant: 'success',
                                            autoHideDuration: 12000,
                                            anchorOrigin: {
                                                vertical: 'bottom',
                                                horizontal: 'right',
                                            },
                                            action: responseAction,
                                        });
                                        handleCloseModal();
                                    }
                                })
                        }}
                    >
                        {(config) => (
                            <Form>
                                <CardHeader
                                    classes={{
                                        root: classes.cardHeaderRoot,
                                        title: classes.header,
                                        action: classes.cardHeaderAction
                                    }}
                                    action={
                                        <IconButton
                                            className={classes.closeButton}
                                            onClick={handleCloseModal}
                                        >
                                            <CloseIcon />
                                        </IconButton>
                                    }
                                    title="Haluatko varata tämän palvelun?"
                                />
                                <Divider />
                                <CardContent
                                    className={classes.cardContent}
                                >
                                    <Grid
                                        className={classes.customGridContainer}
                                        container
                                        spacing={3}
                                    >
                                        <Grid
                                            item
                                            lg={6}
                                            xs={12}
                                        >
                                            {/*<div className={classes.inputWrapper}>*/}
                                            {/*    <div className={classes.inputLabel}>*/}
                                            {/*        <label htmlFor="lastName">Sukunimi *</label>*/}
                                            {/*    </div>*/}
                                            {/*    <div className={classes.inputItem}>*/}
                                            {/*        <VirtualizedList*/}
                                            {/*            config={config}*/}
                                            {/*            prices={prices}*/}
                                            {/*        />*/}
                                            {/*        {config.errors.lastName && config.touched.lastName ? (*/}
                                            {/*            <div className={classes.lastNameError}>{config.errors.lastName}</div>*/}
                                            {/*        ) : null}*/}
                                            {/*    </div>*/}
                                            {/*</div>*/}
                                            <div className={classes.inputRow}>
                                                <InputItem
                                                    id="lastName"
                                                    name="lastName"
                                                    label="Sukunimi *"
                                                    classes={classes}
                                                />
                                                {config.errors.lastName && config.values.lastName ? (
                                                    <div className={classes.error}>{config.errors.lastName}</div>
                                                ) : null}
                                            </div>

                                            <InputItem
                                                // disabled={!config.values.lastName}
                                                id="firstname"
                                                name="firstName"
                                                label="Etunimi *"
                                                classes={classes}
                                            />
                                            {config.errors.firstName && config.values.lastName ? (
                                                <div className={classes.error}>{config.errors.firstName}</div>
                                            ) : null}
                                        </Grid>
                                        <Grid
                                            item
                                            lg={6}
                                            xs={12}
                                        >
                                            <div className={classes.inputRow}>
                                                <InputItem
                                                    // disabled={!config.values.lastName}
                                                    id="number"
                                                    name="number"
                                                    label="Matkapuhelin­numero *"
                                                    hint="kirjoita matkapuhelinnumero kansainvälisessä muodossa (+358xxxxxxxxxx)"
                                                    onBlur={e => {
                                                        let value = e.target.value;
                                                        config.handleBlur(e)
                                                        if (value.charAt(0) === '0') {
                                                            value = `+358${value.substr(1)}`
                                                        }
                                                        config.values.number = value;
                                                        config.validateField('number');
                                                    }}
                                                    classes={classes}
                                                />
                                                {config.errors.number && config.values.lastName ? (
                                                    <div className={classes.error}>{config.errors.number}</div>
                                                ) : null}
                                            </div>


                                            <InputItem
                                                // disabled={!config.values.lastName}
                                                id="email"
                                                name="email"
                                                label="Sähköposti"
                                                classes={classes}
                                            />
                                            {config.errors.email && config.values.lastName && config.values.sendEmail ? (
                                                <div className={classes.error}>{config.errors.email}</div>
                                            ) : null}
                                        </Grid>
                                        <Grid
                                            item
                                            lg={6}
                                            xs={12}
                                        >
                                            <InputItem
                                                // disabled={!config.values.lastName}
                                                id="company"
                                                name="company"
                                                label="Yritys"
                                                classes={classes}
                                            />
                                        </Grid>
                                        <Grid
                                            item
                                            lg={6}
                                            xs={12}
                                        >
                                            <InputItem
                                                // disabled={!config.values.lastName}
                                                component="textarea"
                                                id="additionalInformation"
                                                name="additionalInformation"
                                                label="Lisätieto"
                                                classes={classes}
                                            />
                                        </Grid>
                                    </Grid>
                                </CardContent>
                                <Divider />
                                <CardContent
                                    className={classes.cardContent}
                                >
                                    <Grid
                                        container
                                    >
                                        <FieldArray
                                            name="orderInfo"
                                            render={arrayHelpers => (
                                                <RadioCard
                                                    config={config}
                                                    currentDate={currentDate}
                                                    prices={prices}
                                                    removeSelectedItem={removeSelectedItem}
                                                    arrayHelpers={arrayHelpers}
                                                />
                                            )}
                                        />
                                    </Grid>
                                </CardContent>
                                <Divider />
                                <CardActions className={classes.actions}>
                                    {/*<FormControlLabel*/}
                                    {/*    classes={{*/}
                                    {/*        label: classes.formControlLabelCheckbox*/}
                                    {/*    }}*/}
                                    {/*    control={*/}
                                    {/*        <Checkbox*/}
                                    {/*            style={{*/}
                                    {/*                padding: 5*/}
                                    {/*            }}*/}
                                    {/*            color="default"*/}
                                    {/*            classes={{*/}
                                    {/*                checked: classes.radioChecked*/}
                                    {/*            }}*/}
                                    {/*            checked={config.values.sendEmail}*/}
                                    {/*            name="sendEmail"*/}
                                    {/*            onChange={config.handleChange}*/}
                                    {/*            value={config.values.sendEmail}*/}
                                    {/*        />*/}
                                    {/*    }*/}
                                    {/*    label="Lähettää Vahvistuksen Asiakkaalle"*/}
                                    {/*/>*/}
                                    {requestForm ? (
                                        <CircularProgress
                                            className={classes.progress}
                                            size={28}
                                        />
                                    ) : (
                                            <Button
                                                disabled={!(config.isValid && config.values.lastName)}
                                                classes={{
                                                    root: classes.submit,
                                                    disabled: classes.disabledSubmitButton
                                                }}
                                                type="submit"
                                            >
                                                OK
                                        </Button>
                                        )}
                                </CardActions>
                            </Form>
                        )}
                    </Formik>
                </Card>
            ) : (
                    <div style={{
                        display: 'flex',
                        justifyContent: 'center',
                        alignItems: 'center',
                        height: '100%'
                    }}>
                        <Loader />
                    </div>
                )}

        </Modal>
    )
}

export default OrderForm;