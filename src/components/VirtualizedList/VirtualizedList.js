import React, { useState, useEffect } from 'react';
import useDebounce from '../../custom-hooks/useDebounce';
import Autocomplete from '@material-ui/lab/Autocomplete';
import { createFilterOptions } from '@material-ui/lab/Autocomplete';
import { TextField, CircularProgress, useMediaQuery } from '@material-ui/core';
import { useTheme, makeStyles } from '@material-ui/core/styles';
import { FixedSizeList } from 'react-window';
import api from '../../services/api';

const useStyles = makeStyles(() => ({
	root: {
		fontSize: 16,
		fontFamily: 'MetaPro'
	},
	option: {
		fontSize: 16,
		fontFamily: 'MetaPro',
		'&::before': {
			display: 'none'
		}
	}
}))

const filterOptions = createFilterOptions({
	matchFrom: 'start',
	stringify: option => `${option.Fields.lastname} ${option.Fields.firstname}`
})

function renderRow(props) {
	const { data, index, style } = props;
	return React.cloneElement(data[index], {
		style: {
			overflow: 'hidden',
			textOverflow: 'ellipsis',
			whiteSpace: 'nowrap',
			display: 'block',
			...style,
		},
	});
}

const ListboxComponent = React.forwardRef(function ListboxComponent(props, ref) {
	const { children, ...other } = props;
	const theme = useTheme();
	const smUp = useMediaQuery(theme.breakpoints.up('sm'));
	const itemCount = Array.isArray(children) ? children.length : 0;
	const itemSize = smUp ? 36 : 48;

	const outerElementType = React.useMemo(() => {
		return React.forwardRef((props2, ref2) => <div ref={ref2} {...props2} {...other} />);
	}, []);

	return (
		<div ref={ref}>
			<FixedSizeList
				style={{ padding: 0, height: Math.min(8, itemCount) * itemSize, maxHeight: 'auto' }}
				itemData={children}
				height={250}
				width="100%"
				outerElementType={outerElementType}
				innerElementType="ul"
				itemSize={itemSize}
				overscanCount={5}
				itemCount={itemCount}
			>
				{renderRow}
			</FixedSizeList>
		</div>
	);
});

const VirtualizedList = ({prices, config}) => {
	const classes = useStyles();
	const [open, setOpen] = useState(false);
	const [contacts, setContacts] = useState([]);
	const [loading, setLoading] = useState(false);

	const debounceSearchTerm = useDebounce(config.values.lastName, 500);

	useEffect(() => {
		if (debounceSearchTerm) {
			setLoading(true);

			api.getContacts(debounceSearchTerm)
				.then(contacts => {
					setLoading(false);
					setContacts(contacts.Data);
				})
		} else {
			setContacts([]);
		}
	}, [debounceSearchTerm]);

	return (
		<Autocomplete
			style={{
				width: '100%',
				padding: '4px 6px',
				fontFamily: 'MetaPro',
				fontSize: '16px',
				background: '#fafafa',
				border: '1px solid rgba(0, 0, 0, 0.3)',
				borderRadius: 3,
				color: '#1d1d1b',
				outline: 'none',
			}}
			classes={{
				option: classes.option,
				input: classes.option
			}}
			onChange={(e, value) => {
				if (!value) {
					config.setValues({
						...config.values,
						'firstName': '',
						'lastName': '',
						'number': '',
						'ReserveOwner': '',
						'company': '',
						'Hinta/Osakashinta': 'False'
					})

					return;
				}
				const { firstname, lastname, telephone1, parentcustomerid, uds_isshareowner } = value.Fields;
				const shareowner = uds_isshareowner === 'False' ? 'Hinta' : 'Osakashinta';
				const mapedOrderInfo = config.values.orderInfo.map(item => {
					const [currentPrice] = prices['Prices'][item.TimeSlotId].filter(v => v.PricingType === shareowner);

					return {
						...item,
						'Cost': `${currentPrice.Price} ${currentPrice.PricingType}`
					}
				});
				config.setValues({
					...config.values,
					'firstName': firstname,
					'lastName': lastname,
					'company': parentcustomerid || '',
					'number': telephone1,
					'ReserveOwner': value.Id || '',
					'Hinta/Osakashinta': uds_isshareowner,
					'orderInfo': mapedOrderInfo,
				})
			}}
			open={open}
			onOpen={() => {
				setOpen(true)
			}}
			onClose={() => {
				setOpen(false);
			}}
			filterOptions={filterOptions}
			getOptionLabel={option => `${option.Fields.lastname}`}
			renderOption={option => (
				`${option.Fields.lastname} ${option.Fields.firstname}`
			)}
			onInputChange={(e, v) => config.setFieldValue('lastName', v)}
			disableListWrap
			ListboxComponent={ListboxComponent}
			options={contacts}
			loading={loading}
			freeSolo
			renderInput={params => {
				return (
					<TextField
						{...params}
						variant="standard"
						classes={{ root: classes.root }}
						fullWidth
						InputProps={{
							...params.InputProps,
							disableUnderline: true,
							style: {
								paddingRight: 30
							},
							endAdornment: (
								<>
									{loading ? <CircularProgress color="inherit" size={20} /> : null}
									{params.InputProps.endAdornment}
								</>
							),
						}}
					/>
				)
			}}
		/>
	)
}

export default VirtualizedList;