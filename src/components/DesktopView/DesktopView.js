import React from 'react';
import NavigateNextIcon from '@material-ui/icons/NavigateNext';
import NavigateBeforeIcon from '@material-ui/icons/NavigateBefore';
import {
    IconButton,
    Typography,
    Button
} from '@material-ui/core';
import {
    KeyboardDatePicker,
} from '@material-ui/pickers';
import { groups } from '../../data-model/config';
import { makeStyles } from '@material-ui/core/styles';
import moment from 'moment';
import {PriceFixReducer, TimeSlotFixReducer} from '../../utils';

const useStyles = makeStyles({
    headerTop: {
        display: 'flex',
    },
    headerTopLeft: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        width: 175,
        padding: 10,
        borderRight: '1px solid #c5c5c5',
        borderLeft: '1px solid #c5c5c5',
        borderBottom: 'none',
        textAlign: 'center'
    },
    headerTopRight: {
        display: 'flex',
        alignItems: 'center',
        width: 'calc(100% - 175px)',
        padding: 10,
        borderBottom: 'none',
        borderLeft: 'none'
    },
    headerBottom: {
        display: 'flex',
    },
    headerBottomLeft: {
        width: 175,
        padding: 10,
        border: '1px solid #c5c5c5',
        background: 'rgba(0, 0, 0, 0.1)',
        borderTop: 'none',
        textAlign: 'center'
    },
    headerBottomRight: {
        display: 'flex',
        width: 'calc(100% - 175px)',
        borderBottom: '1px solid #c5c5c5',
        borderTop: 'none',
    },
    itemsRoot: {
        display: 'flex',
        flexDirection: 'column'
    },
    itemsSideBar: {
        width: 175,
        borderRight: '1px solid #c5c5c5',
        borderBottom: '1px solid #c5c5c5',
        textAlign: 'center'
    },
    itemsContainer: {
        width: 'calc(100% - 175px)'
    },
    icons: {
        padding: 3,
        borderRadius: 3,
        background: '#c00000',
        color: '#fff',
        transition: '.2s ease-in-out',
        '&:hover': {
            background: '#cd3333'
        },
        '&.Mui-disabled': {
            background: 'rgb(192, 0, 0)',
            color: '#fff',
            opacity: '0.3'
        }
    },
    dataText: {
        padding: 5,
        fontFamily: 'MetaPro',
        fontSize: '16px',
        lineHeight: '20px'
    },
    calendarInput: {
        fontFamily: 'MetaPro',
        fontSize: '16px',
        lineHeight: '20px'
    },
    todayButton: {
        marginLeft: 10,
        fontFamily: 'MetaPro',
        fontSize: '16px',
        lineHeight: '20px',
        background: '#c00000',
        color: '#fff',
        transition: '.2s ease-in-out',
        '&:hover': {
            background: '#cd3333'
        }
    },
    inputRoot: {
        fontFamily: 'MetaPro',
        fontSize: '16px',
        lineHeight: '20px',
    },
    underline: {
        transition: '.2s ease-in-out',
        '&:hover:not(.Mui-disabled):before': {
            borderColor: '#cd3333'
        },
        '&::after': {
            borderColor: '#c00000'
        }
    },
    popoverRoot: {
        '& .MuiPickersBasePicker-pickerView': {
            maxWidth: '285px',
            minWidth: '275px',
            padding: '0 8px'
        },
        '& .MuiIconButton-root': {
            padding: 3
        },
        '& .MuiTypography-body1': {
            fontFamily: 'MetaPro',
            fontSize: '16px',
            lineHeight: '20px',
        },
        '& .MuiTypography-caption': {
            fontFamily: 'MetaPro',
            fontSize: '16px',
            lineHeight: '20px',
        },
        '& .MuiTypography-body2': {
            fontFamily: 'MetaPro',
            fontSize: '16px',
            lineHeight: '20px',
        },
        '& .MuiPickersDay-daySelected': {
            background: '#c00000',
        },
        '& .MuiPickersDay-current.MuiPickersDay-daySelected': {
            color: '#fff',
            background: '#c00000',
        },
        '& .MuiPickersDay-current': {
            color: '#c00000'
        }
    },
    formBtn: {
        padding: '4px 8px',
        fontSize: 16,
        fontFamily: 'MetaPro',
        background: '#c00000',
        color: '#fff',
        textAlign: 'center'
    },
    headerTopRightContainer: {
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingRight: 10,
        border: '1px solid #c5c5c5',
        borderLeft: 'none'
    },
    rowRight: {
        display: 'flex',
        width: 'calc(100% - 175px)',
        '&:hover': {
            '&>div': {
                background: '#d8d8d8'
            }
        }
    }
})

function createTimeLine(from, to) {
    const times = [];
    const width = 100 / (to - from);

    for (let i = from; i <= to; i++) {
        const t = i < 10 ? `0${i}` : i;
        times.push((
            <div
                key={t}
                style={{
                    width: `${width}%`,
                    padding: 10,
                    background: i % 2 === 0 ? '#fff' : 'rgba(0, 0, 0, 0.1)',
                    borderRight: '1px solid #c5c5c5',
                    textAlign: 'center'
                }}
            >
                {`${t}.00`}
            </div>
        ))
    }

    return times
}

const DesktopView = (props) => {
    const {
        to,
        from,
        data,
        isToday,
        maxDate,
        setPrevDay,
        setNextDay,
        setTodayDay,
        currentDate,
        selectedItems,
        handleOpenModal,
        handleDateChange,
        handleSelectedItems,
    } = props;
    const classes = useStyles();
    const timeSlotMap = new Map();

    data.forEach(item => {
        item = {...TimeSlotFixReducer(item)};

        if (item.available && parseInt(item.StartTime) >= from && parseInt(item.EndTime) <= to) {
            const key = item.locationInfo.Title === 'Jopo' ? item.Resources[0].Name : item.locationInfo.Title;
            const value = timeSlotMap.get(key);
            timeSlotMap.set(key, !value ? [item] : [...value, item]);
        }
    });

    const createSlots = items => {
        const timeMap = new Map();
        items.forEach(s => {
            const key = s.StartTime;
            const value = timeMap.get(key);
            timeMap.set(key, !value ? [s] : [...value, s]);
        })

        const results = [];

        for (let timeMapItems of timeMap.values()) {
            timeMapItems[0] = PriceFixReducer(timeMapItems[0]);

            const activeSlots = timeMapItems.filter(timeMapItem => {
                const ct = currentDate.format('HH.mm');
                const today = moment();

                console.log('today.isAfter(maxDate)',today.isAfter(maxDate))

                if (today.isAfter(maxDate)) {
                    return false
                }

                if (currentDate.isSame(maxDate, 'day')) {
                    return timeMapItem.Status === 1 && timeMapItem.StartTime < 12;
                }

                if (currentDate.isSame(today, 'day')) {
                    return timeMapItem.Status === 1 && ct < timeMapItem.StartTime;
                }

                return timeMapItem.Status === 1;
            });
            const isSlotActive = activeSlots.length !== 0;
            const text = isSlotActive ? (timeMapItems[0].StandardPriceStr === 0 ? null : timeMapItems[0].StandardPriceStr) : null;
            const isItemSelected = selectedItems.filter(selectedItem => {
                return timeMapItems.filter(timeMapItem => timeMapItem.TimeSlotId === selectedItem.TimeSlotId).length !== 0;
            }).length !== 0;

            results.push(
                {
                    'StartTime': timeMapItems[0].StartTime,
                    'EndTime': timeMapItems[0].EndTime,
                    text,
                    isSlotActive,
                    isItemSelected,
                    pickedSlot: isSlotActive ? [activeSlots[0]] : []
                }
            )
        }

        return results.map((item, idx) => {
            const text = item.text;
            const isSlotActive = item.isSlotActive;
            const width = 100 / (to - from);
            const isItemSelected = item.isItemSelected;

            return (
                <div
                    key={idx}
                    style={{
                        display: 'flex',
                        justifyContent: 'center',
                        alignItems: 'center',
                        width: `${width}%`,
                        padding: 6,
                        borderRight: '1px solid #c5c5c5',
                        borderBottom: '1px solid #c5c5c5',
                        background: isItemSelected ? '#c00000' : (isSlotActive ? '#fff' : '#c5c5c5'),
                        color: isItemSelected ? '#fff' : 'inherit',
                        cursor: isSlotActive ? 'pointer': 'default'
                    }}
                    onClick={isSlotActive ? () => handleSelectedItems(item.pickedSlot[0], isItemSelected) : null}
                >
                    {text}
                </div>
            )
        })
    }

    const res = groups.map((group, idx) => {
        const items = timeSlotMap.get(group.value);

        if (!items) {
            return;
        }

        return (
            <div
                key={group.title}
                style={{
                    display: 'flex',
                }}
            >
                <div
                    style={{
                        width: 175,
                        padding: 10,
                        background: idx % 2 === 0 ? '#fff' : 'rgba(0, 0, 0, 0.1)',
                        borderRight: '1px solid #c5c5c5',
                        borderBottom: '1px solid #c5c5c5',
                        borderLeft: '1px solid #c5c5c5'
                    }}
                >
                    {group.title}
                </div>
                <div
                    className={classes.rowRight}
                >
                    {createSlots(items)}
                </div>
            </div>
        )
    })

    return (
        <div>
            <div className="header">
                <div
                    className={classes.headerTopRightContainer}
                >
                    <div className={classes.headerTop}>
                        <div className={classes.headerTopLeft}>
                            Valitse haluamasi palvelu ja ajankohta kalenterista
							          </div>
                        <div className={classes.headerTopRight}>
                            <div style={{
                                display: 'flex',
                                alignItems: 'center',
                                marginRight: 10
                            }}>
                                <IconButton
                                    className={classes.icons}
                                    onClick={setPrevDay}
                                    disabled={moment().format('YYYY-MM-DD') === currentDate.format('YYYY-MM-DD')}
                                >
                                    <NavigateBeforeIcon />
                                </IconButton>
                                <Typography
                                    className={classes.dataText}
                                >
                                    {currentDate.format('dddd DD MMMM YYYY')}
                                </Typography>
                                <IconButton
                                    className={classes.icons}
                                    onClick={setNextDay}
                                    disabled={currentDate.isSameOrAfter(maxDate)}
                                >
                                    <NavigateNextIcon />
                                </IconButton>
                            </div>
                            <KeyboardDatePicker
                                autoOk
                                value={currentDate}
                                maxDate={maxDate}
                                disablePast
                                onChange={handleDateChange}
                                shouldDisableDate={day => day.isAfter(maxDate)}
                                variant="inline"
                                InputProps={{
                                    classes: {
                                        root: classes.inputRoot,
                                        underline: classes.underline
                                    }
                                }}
                                PopoverProps={{
                                    classes: {
                                        paper: classes.popoverRoot
                                    }
                                }}
                                disableToolbar
                            />
                            {!isToday ? (
                                <Button
                                    className={classes.todayButton}
                                    onClick={setTodayDay}
                                >
                                    Takaisin tähän päivään
									              </Button>
                            ) : null}
                        </div>
                    </div>
                    {selectedItems.length ? (
                        <Button
                            className={classes.formBtn}
                            onClick={handleOpenModal}
                        >
                            Varaa
                        </Button>
                    ) : null}
                </div>
                <div className={classes.headerBottom}>
                    <div className={classes.headerBottomLeft}></div>
                    <div className={classes.headerBottomRight}>
                        {createTimeLine(from, to)}
                    </div>
                </div>
            </div>
            <div className={classes.itemsRoot}>
                {res}
            </div>
        </div>
    )
}

export default DesktopView;
