import React, { useState } from 'react';
import {
    Select,
    MenuItem,
    Button
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { groups } from '../../data-model/config';
import {
    KeyboardDatePicker,
} from '@material-ui/pickers';
import {PriceFixReducer, TimeSlotFixReducer} from '../../utils';
import moment from 'moment';

const useStyles = makeStyles(() => ({
    headerTop: {
        display: 'flex'
    },
    headerTopLeft: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        width: 120,
        padding: '15px 10px',
        border: '1px solid #c5c5c5',
        borderBottom: 'none'
    },
    headerTopRight: {
        display: 'flex',
        alignItems: 'center',
        width: 'calc(100% - 120px)',
        padding: '15px 10px',
        border: '1px solid #c5c5c5',
        borderLeft: 'none',
        borderBottom: 'none',
        textAlign: 'center'
    },
    headerBottom: {
        display: 'flex'
    },
    headerBottomLeft: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        width: 120,
        padding: '15px 10px',
        background: 'rgba(0, 0, 0, 0.1)',
        border: '1px solid #c5c5c5'
    },
    headerBottomRight: {
        width: 'calc(100% - 120px)',
        padding: '15px 10px',
        border: '1px solid #c5c5c5',
        borderLeft: 'none',
        textAlign: 'center'
    },
    itemsContainer: {
        display: 'flex',
    },
    itemsSidebar: {
        width: 120,
        padding: '15px 10px',
        border: '1px solid #c5c5c5',
        borderTop: 'none',
        textAlign: 'center'
    },
    itemsContent: {
        width: 'calc(100% - 120px)',
        padding: '15px 10px',
        border: '1px solid #c5c5c5',
        borderTop: 'none',
        borderLeft: 'none',
        textAlign: 'center'
    },
    select: {
        width: '100%',
        fontFamily: 'MetaPro',
        fontSize: '16px',
        lineHeight: '20px',
        '&::after': {
            borderBottomColor: 'rgb(192, 0, 0)'
        }
    },
    menuItem: {
        fontFamily: 'MetaPro',
        fontSize: '16px',
        lineHeight: '20px',
    },
    calendarInput: {
        fontFamily: 'MetaPro',
        fontSize: '16px',
        lineHeight: '20px',
        textAlign: 'center',
    },
    inputRoot: {
        width: '100%',
        fontFamily: 'MetaPro',
        fontSize: '16px',
        lineHeight: '20px',
        '& .MuiInputBase-input': {
            textAlign: 'center'
        }
    },
    underline: {
        transition: '.2s ease-in-out',
        '&:hover:not(.Mui-disabled):before': {
            borderColor: '#cd3333'
        },
        '&::after': {
            borderColor: '#c00000'
        }
    },
    popoverRoot: {
        '& .MuiPickersBasePicker-pickerView': {
            maxWidth: '285px',
            minWidth: '275px'
        },
        '& .MuiIconButton-root': {
            padding: 5
        },
        '& .MuiTypography-body1': {
            fontFamily: 'MetaPro',
            fontSize: '16px',
            lineHeight: '20px',
        },
        '& .MuiTypography-caption': {
            fontFamily: 'MetaPro',
            fontSize: '16px',
            lineHeight: '20px',
        },
        '& .MuiTypography-body2': {
            fontFamily: 'MetaPro',
            fontSize: '16px',
            lineHeight: '20px',
        },
        '& .MuiPickersDay-daySelected': {
            background: '#c00000',
        },
        '& .MuiPickersDay-current.MuiPickersDay-daySelected': {
            color: '#fff',
            background: '#c00000',
        },
        '& .MuiPickersDay-current': {
            color: '#c00000'
        }
    },
    selectUnderline: {
        '&::after': {
            borderColor: '#c00000'
        }
    },
    formBtn: {
        position: 'fixed',
        top: '50%',
        right: -14,
        padding: '4px 8px',
        fontSize: 16,
        fontFamily: 'MetaPro',
        background: 'rgb(62, 25, 25)',
        color: '#fff',
        textAlign: 'center',
        transform: 'translateY(-50%) rotate(-90deg)',
        '&:hover': {
            background: 'rgb(62, 25, 25)',
        }
    },
}))

// const

const TabletAndMobileView = (props) => {
    const {
        to,
        from,
        data,
        maxDate,
        currentDate,
        selectedItems,
        handleOpenModal,
        handleDateChange,
        handleSelectedItems,
    } = props;
    const classes = useStyles();

    let [currentGroup, setCurrentGroup] = useState(groups[0]);
    const serviceGroups = [];
    const timeSlotMap = new Map();
    data.forEach(item => {
        item = {...TimeSlotFixReducer(item)};

        const key = item.locationInfo.Title === 'Jopo' ? item.Resources[0].Name : item.locationInfo.Title;
        const value = timeSlotMap.get(key);

        if (!value) {
            const group = groups.find(group => group.value === key);

            if (group) {
                serviceGroups.push(group);
            }
        }

        if (item.available) {
            timeSlotMap.set(key, !value ? [item] : [...value, item]);
        }
    });

    serviceGroups.sort((a,b) => {
        return a.id - b.id;
    });

    let currentGroupItems = timeSlotMap.get(currentGroup.value);

    if (!currentGroupItems) {
        setCurrentGroup(serviceGroups[0]);
    }

    const timeMap = new Map();
    let results = [];

    if (currentGroupItems) {
        currentGroupItems.forEach((item, idx) => {
            const value = timeMap.get(item.StartTime);
            if (parseInt(item.StartTime, 10) >= from && parseInt(item.EndTime, 10) <= to) {
                timeMap.set(item.StartTime, !value ? [item] : [...value, item]);
            }
        })

        for (let key of timeMap.keys()) {
            const items = timeMap.get(key);
            items[0] = PriceFixReducer(items[0]);
            // const activeSlots = items.filter(item => item.Status === 1);
            const activeSlots = items.filter(timeMapItem => {
                const ct = currentDate.format('HH.mm');
                const today = moment();

                if (today.isAfter(maxDate)) {
                    return false
                }

                if (currentDate.isSame(maxDate, 'day')) {
                    return timeMapItem.Status === 1 && timeMapItem.StartTime < 12;
                }

                if (currentDate.isSame(today, 'day')) {
                    return timeMapItem.Status === 1 && ct < timeMapItem.StartTime;
                }

                return timeMapItem.Status === 1;
            });
            const isSlotActive = !!activeSlots.length;
            const text = isSlotActive ? (items[0].StandardPriceStr === 0 ? '' : items[0].StandardPriceStr) : '';
            const isItemSelected = selectedItems.filter(selectedItem => {
                return items.filter(item => item.TimeSlotId === selectedItem.TimeSlotId).length !== 0;
            }).length !== 0;

            results.push(
                <div
                    key={key}
                    className={classes.itemsContainer}
                >
                    <div
                        className={classes.itemsSidebar}
                    >
                        {key}
                    </div>
                    <div
                        className={classes.itemsContent}
                        style={{
                            background: isItemSelected ? '#c00000' : (isSlotActive ? '#fff' : '#c5c5c5'),
                            color: isItemSelected ? '#fff' : 'inherit'
                        }}
                        onClick={isSlotActive ? () => handleSelectedItems(activeSlots[0], isItemSelected) : () => {}}
                    >
                        {text}
                    </div>
                </div>
            )
        }
    }

    const handleGroupChange = (event) => {
        const [item] = groups.filter(group => group.title === event.target.value);
        setCurrentGroup(item);
    }

    return (
        <div>
            <div className={classes.headerTop}>
                <div className={classes.headerTopLeft}>
                    Valitse haluamasi palvelu ja ajankohta kalenterista
                </div>
                <div className={classes.headerTopRight}>
                    <KeyboardDatePicker
                        autoOk
                        maxDate={maxDate}
                        disablePast
                        value={currentDate}
                        format="DD.MM.YYYY"
                        onChange={handleDateChange}
                        shouldDisableDate={day => day.isAfter(maxDate)}
                        variant="inline"
                        InputProps={{
                            classes: {
                                root: classes.inputRoot,
                                underline: classes.underline
                            }
                        }}
                        PopoverProps={{
                            classes: {
                                paper: classes.popoverRoot
                            }
                        }}
                        disableToolbar
                    />
                </div>
            </div>
            <div className={classes.headerBottom}>
                <div className={classes.headerBottomLeft}></div>
                <div className={classes.headerBottomRight}>
                    <Select
                        className={classes.select}
                        onChange={handleGroupChange}
                        value={currentGroup.title}
                    >
                        {serviceGroups.map(group => {
                            return (
                                <MenuItem
                                    className={classes.menuItem}
                                    key={group.id}
                                    value={group.title}
                                >
                                    {group.title}
                                </MenuItem>
                            )
                        })}
                    </Select>
                </div>
            </div>
            <div>
                {results}
            </div>
            {selectedItems.length ? (
                <Button
                    className={classes.formBtn}
                    onClick={handleOpenModal}
                >
                    Varaa
                </Button>
            ) : null}
        </div>
    )
}

export default TabletAndMobileView;
