import React from 'react';
import { Field } from 'formik';

const InputItem = ({ id, label, name, disabled = false, classes, component = "input", hint = false, validate = () => {}, ...props}) => {
  return (
    <div className={classes.inputWrapper}>
        <div className={classes.inputLabel}>
            <label htmlFor={id}>{label}</label>
        </div>
        <div className={classes.inputItem}>
          {hint ? (<div className={classes.inputHint}>{hint}</div>) : null }
            <Field
                disabled={disabled}
                component={component}
                autoComplete="off"
                className={classes.inputStyle}
                validate={validate}
                style={{
                    border: '1px solid rgba(0, 0, 0, 0.3)',
                    resize: 'none'
                }}
                id={id}
                name={name}
                {...props}
            />
        </div>
    </div>
)}

export default InputItem;