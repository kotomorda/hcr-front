import React from 'react';
import './loader.css';

const Loader = () => (
    <div className="loadingio-spinner-ellipsis-7tigfk2cggw">
        <div className="ldio-fhyz1gceyev">
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
        </div>
    </div>
)

export default Loader;