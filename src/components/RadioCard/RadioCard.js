import React from 'react';
import {
    IconButton,
    RadioGroup,
    Radio,
    FormControlLabel,
    makeStyles
} from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import moment from 'moment';

const useStyles = makeStyles(theme => ({
    itemCard: {
        position: 'relative',
        width: 220,
        margin: 10,
        padding: 10,
        border: '2px dotted #e5e5e5',
    },
    itemCardHeader: {
        display: 'flex',
    },
    radio: {
        padding: 2,
        color: '#c5c5c5'
    },
    radioChecked: {
        color: '#999'
    },
    formControlLabel: {
        fontSize: 18,
        fontFamily: 'MetaPro'
    },
    formControlLabelRoot: {
        margin: 0,
        flexBasis: '50%'
    },
    cardHeaderTitle: {
        margin: 0,
        marginRight: 10,
        fontSize: 16,
        fontWeight: 'bold',
        color: '#747479',
        textTransform: 'capitalize'
    },
    cardDateTitle: {
        textTransform: 'capitalize'
    },
    closeBtn: {
        position: 'absolute',
        top: 0,
        right: 0,
        padding: 3
    },
    radioGroup: {
        justifyContent: 'space-between',
        flexDirection: 'row'
    },
    itemCardContainer: {
        display: 'flex',
        flexWrap: 'wrap',
        width: '100%',
        [theme.breakpoints.down('sm')]: {
            justifyContent: 'center'
        }
    },
}))

const RadioCard = ({currentDate, config, removeSelectedItem, prices, arrayHelpers}) => {
    const classes = useStyles();

    return (
        <div style={{
            display: 'flex',
            flexDirection: 'column',
            width: '100%'
        }}>
            <div className={classes.itemCardHeader}>
                <p className={classes.cardHeaderTitle}>
                    Yhteensä:
                    {" "}
                    {config.values.orderInfo.reduce((s, c) => {
                        return s + parseInt(c.Cost)
                    }, 0)} €
                </p>
            </div>
            <div className={classes.itemCardContainer}>
                {config.values.orderInfo.map((item, index) => {
                    let title = item.Resources[0].Name;
                    if (item.locationInfo.title !== 'Jopo') {
                        const titleArray = item.Resources[0].Name.split(' ');
                        if (!isNaN(parseInt(titleArray[titleArray.length - 1], 10))) {
                            title = titleArray.splice(0, titleArray.length - 1).join(' ')
                        }
                    }

                    let price = prices['Prices'][item.TimeSlotId];

                    if (item.locationInfo.title === 'Padel outside') {
                        if (parseInt(item.StartTime) < 15) {
                            price = prices['Prices'][item.TimeSlotId].filter(el => el.Price === 26);
                        } else {
                            price = prices['Prices'][item.TimeSlotId].filter(el => el.Price === 30);
                        }
                    }

                    const name = `orderInfo[${index}].Cost`;
                    return (
                        <div className={classes.itemCard} key={item.TimeSlotId}>
                            <IconButton
                                className={classes.closeBtn}
                                onClick={() => {
                                    removeSelectedItem(item.TimeSlotId);
                                    arrayHelpers.remove(index);
                                }}
                            >
                                <CloseIcon />
                            </IconButton>
                            <p>{title}</p>
                            <p className={classes.cardDateTitle}>{moment(item.Date).format('dddd DD MMMM YYYY')} Klo {`${item.StartTime} - ${item.EndTime}`}</p>
                            {item.locationInfo.title === 'Jopo'
                                ?
                                <p>15€ 3h</p>
                                :
                                <RadioGroup
                                    className={classes.radioGroup}
                                    key={item.TimeSlotId}
                                    value={item.Cost}
                                    name={name}
                                    onChange={(e) => config.setFieldValue(name, e.target.value)}
                                >
                                    {price.map(item => (
                                        <div
                                            key={item.PricingType}
                                            style={{
                                                display: 'flex',
                                                flexDirection: 'column',
                                            }}
                                        >
                                            <FormControlLabel
                                                classes={{
                                                    root: classes.formControlLabelRoot,
                                                    label: classes.formControlLabel
                                                }}
                                                control={
                                                    <Radio
                                                        classes={{
                                                            root: classes.radio,
                                                            checked: classes.radioChecked
                                                        }}
                                                        color="default"
                                                    />
                                                }
                                                label={item.Price ? `${item.Price} ${item.PriceRepresentation}` : 'Voimassa olevan hinnaston mukaan'}
                                                value={`${item.Price} ${item.PricingType}`}
                                            />
                                            <span style={{ fontSize: 14 }}>{item.PricingType}</span>
                                            {item.PricingType === "Hohtokeilailu" && <span style={{ fontSize: 12 }}>Hohtokeilailua keskiviikkona, perjantaina ja lauantaina klo 21.00</span>}
                                        </div>
                                    ))}
                                </RadioGroup>}
                        </div>
                    )
                })}
            </div>
        </div>
    )
};

export default RadioCard;
