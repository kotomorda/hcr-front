const productionEnv = window.location.hostname === 'hcrcalendar.varaa.online';
let _URL = productionEnv ? 'https://harraste.varaa.online/Free' : 'https://holidayclub-qa.azurewebsites.net/Free';

class Api {
    getData = async (startDate, endDate) => {
        const fullURL = `${_URL}/Reservation/Calendar?startDate=${startDate}&endDate=${endDate}`;

        const res = await fetch(fullURL, {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'cache-control': 'no-cache',
            }
        });

        if (!res.ok) {
            throw new Error(`Couldn't fetch ${fullURL}, received ${res.status}`)
        }

        return await res.json();
    }

    getPrices = async (ids) => {
        const fullURL = `${_URL}/Prices?timeSlotIds=${ids}`;

        const res = await fetch(fullURL, {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'cache-control': 'no-cache',
            }
        });

        if (!res.ok) {
            throw new Error(`Couldn't fetch ${fullURL}, received ${res.status}`)
        }

        return await res.json();
    }

    getContacts = async (searchStr) => {
        const fullURL = `${_URL}/Contact?name=${searchStr}`;

        const res = await fetch(fullURL, {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'cache-control': 'no-cache',
            }
        });

        if (!res.ok) {
            throw new Error(`Couldn't fetch ${fullURL}, received ${res.status}`)
        }

        return await res.json();
    }

    requestForm = async (data) => {
        const fullURL = `${_URL}/Reservation/Reserve`;

        const res = await fetch(fullURL, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'cache-control': 'no-cache',
            },
            body: JSON.stringify(data)
        })

        if (!res.ok) {
            throw new Error(`Couldn't send ${fullURL}, received ${res.status}`)
        }

        return await res.json();
    }
}

export default new Api();